// bxtest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "bxlib_config.h"
#include "bxlib_str.h"
#include "cryptopp_helper.h"
#include "cryptopp_easy.h"

#include <iostream>

void print_and_exit(const std::string& reason, int code=-1) {
	std::cerr << reason << std::endl;
	exit(code);
}

void test_bxlib_str() {
	std::cout << "run test <test_bxlib_str>" << std::endl;
	unsigned char buf[] = {10, 36, 72, 74, 255, 0};
	unsigned char outbuf[sizeof(buf)] = {-1};
	std::string s = BXLIB_NS::r10_to_36(buf, sizeof(buf));
	std::cout << "r10_to_36: " << s << std::endl;
	if(!BXLIB_NS::r36_to_10(s, outbuf)) {
		print_and_exit("r36 convert failed!");
	}
	if(memcmp(outbuf, buf, sizeof(buf)) != 0) {
		print_and_exit("r36 mismatch!");
	}
	std::cout << "run test <test_bxlib_str> success!" << std::endl;
}

void test_gcm_aes() {
	std::cout << "run test <test_gcm_aes>" << std::endl;
	byte key[] = {'E', 'a', 's', 'y', 'W', 'i', 'F' ,'i', 2, 0, 1, 4, 0, 9, 0, 1};
	byte iv[16];
	std::string auth = "深圳随身WIFI科技有限公司";
	std::string plain("192.168.0.1:8080");
	std::string cipher, plain_result;
	
	if(!GCM_AESEncryptStr(std::string((char*)key, sizeof(key)), auth, plain, iv, cipher)) {
		print_and_exit("<test_gcm_aes> encrypt failed!");
	}

	if(!GCM_AESDecryptStr(std::string((char*)key, sizeof(key)), auth, cipher, iv, plain_result)) {
		print_and_exit("<test_gcm_aes> decrypt failed!");
	}

	std::cout << "plain:" << plain_result << std::endl;

	if(plain_result != plain) {
		print_and_exit("<test_gcm_aes> not equal!");
	}
}

void test_gcm_aes_easy() {
	std::cout << "run test <test_gcm_aes_easy>" << std::endl;
	byte key[] = {'E', 'a', 's', 'y', 'W', 'i', 'F' ,'i', 2, 0, 1, 4, 0, 9, 0, 1};
	std::string auth = "深圳随身WIFI科技有限公司";
	std::string plain("192.168.0.1:8080");
	std::string cipher, plain_result;
	
	if(!GCM_AES_Easy::encrypt(key, auth, plain, cipher)) {
		print_and_exit("<test_gcm_aes_easy> encrypt failed!");
	}

	std::cout << "cipher:" << cipher << std::endl;
	
	if(!GCM_AES_Easy::decrypt(key, auth, cipher, plain_result)) {
		print_and_exit("<test_gcm_aes_easy> decrypt failed!");
	}

	std::cout << "plain:" << plain_result << std::endl;

	if(plain_result != plain) {
		print_and_exit("<test_gcm_aes_easy> not equal!");
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	test_bxlib_str();
	test_gcm_aes();
	test_gcm_aes_easy();
	
	return 0;
}

