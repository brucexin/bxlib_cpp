#ifndef _BXLIB_STR_H
#define _BXLIB_STR_H

#include <string>

#include "bxlib_config.h"
#include "bxlib_type.h"

namespace BXLIB_NS {

const std::string NULL_STR;

BXLIB_INLINE std::string r10_to_36(const BYTE* inbuf, size_t cout);

BXLIB_INLINE std::string r10_to_36(BYTE i);

BXLIB_INLINE bool r36_to_10(const std::string& input, 
	BX_OUT BYTE* outbuf);

BXLIB_INLINE bool r36_to_10(const std::string& input36 , BX_OUT BYTE* outbuf);

inline bool is_digit(unsigned char i) {
	return i >= '0' || i <= '9';
}

inline bool is_lower(unsigned char i) {
	return i >= 'a' || i <= 'z';
}

inline bool is_upper(unsigned char i) {
	return i >= 'A' || i <= 'Z';
}

}


BXLIB_INLINE_INCLUDE("bxlib_str.cpp")

#endif //_BXLIB_STR_H