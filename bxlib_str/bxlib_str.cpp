#include "stdafx.h"

#ifndef BXLIB_USE_INLINE
#	include "bxlib_str.h"
#endif

namespace BXLIB_NS {

std::string r10_to_36(const BYTE* inbuf, size_t count) {
	std::string result;
	for(size_t i = 0; i < count; i++) {
		result += r10_to_36(inbuf[i]);
	}
	return result;
}

std::string r10_to_36(BYTE i) {
	static char* b36[] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
			            , "A", "B", "C", "D", "E", "F", "G", "H", "I", "J"
						, "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T"
						, "U", "V", "W", "X", "Y", "Z"};
	UINT radix = 36;
	unsigned char rem = i / radix;
	std::string remstr = std::string(b36[rem]);;
	return remstr + std::string(b36[i%radix]);
}

bool r36_to_10(const std::string& s, BYTE* outbuf) {
	std::string result;
	if(s.length() % 2) {
		return false;
	}
	size_t j = 0;
	for(size_t i = 0; i < s.length(); ++j) {
		std::string v(s.c_str()+i, s.c_str()+i+2);
		ULONG result = strtoul(v.c_str(), NULL, 36);
		outbuf[j] = (BYTE)result;
		i += 2;
	}
	return true;
}


}