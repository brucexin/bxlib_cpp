#ifndef _DIGEST_H
#define _DIGEST_H

#include <string>
#include "bxlib_config.h"

namespace BXLIB_NS {

BXLIB_INLINE void hmac(const std::string& input, std::string& key, std::string& output, size_t key_size);

}

BXLIB_INLINE_INCLUDE("bxlib_str.cpp")


#endif //_DIGEST_H