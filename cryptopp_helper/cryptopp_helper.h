    #pragma once  
      
    /* 
    本模块功能： 
     AES加解密,SHA256生成内容文摘，RSA非对称加解密。 
    測試環境： 
     [1]VS2008 SP1 
     [2]WinXP SP3 
     [3]cryptopp561 
    測試時間： 
     [1]2012-7 by kagula 
    更新记录： 
     [1]2012-10 修正AES加解密时key长度没有对齐的问题 
    备注： 
     [1]cryptopp三要素：XXXSource指的是源，Filter指的是过滤器，XXXSink是容器。 
     [2]AES加密产生的密文以零结尾? 密文跟明文的长度一样？ 
     [3]要二进制编码的话参考testAES函数。 
    参考资料 
     [1]Sink的概念 
     http://www.cryptopp.com/wiki/Sink 
     [2]块为单位编解码 
     http://topic.csdn.net/u/20070512/19/297431db-3b82-480d-96a7-9101d4abf13c.html 
     [3]GCM模式 
     http://www.cryptopp.com/wiki/GCM    
     [4]Crypto++库在VS 2005中的使用——RSA加解密 
     http://www.cnblogs.com/cxun/archive/2008/07/30/743541.html 
     [5]CryptoPP 5.6.1 SHA256 results incorrect with 32 bit MSVC 2005 release build 
     http://sourceforge.net/apps/trac/cryptopp/ticket/7 
     [6]使用cryptopp实现密钥协商 
     http://bbs.gameres.com/thread_118476.html 
     [7]AES对称加密算法原理 
     http://www.2cto.com/Article/201112/113465.html 
    相关： 
     高级加密标准（Advanced Encryption Standard，AES），在密码学中又称Rijndael加密法， 
    是美国联邦政府采用的一种区块加密标准。这个标准用来替代原先的DES，传递非机密文件。 
    */  
      
    #include <string>  
    #include <config.h> //byte  
      
    //////////////////////////////////////////////////////////  
    /* 
    功能 
     二进制数据块，转，16进制字符串 
    入口 
     s     二进制数据块 
     s_len 二进制数据块 长度 
    出口 
     16进制ASCII码字符串 
    */  
    std::string Bytes2Hex(byte * s,unsigned int s_len);  
    /* 
    功能 
     16进制字符串，转，二进制数据块 
    入口 
     encoded     16进制ASCII码字符串 
    出口 
     d     二进制数据块 
     d_len 二进制数据块的长度 
    */  
    bool Hex2Bytes(std::string encoded,byte *d,unsigned int &d_len);  
    //////////////////////////////////////////////////////////  
    /* 
    功能 
     CFB模式AES算法字符串加密 
    入口 
     sKey      钥匙 
     plainText 明文 
    出口 
     密文  
    */  
    std::string CFB_AESEncryptStr(std::string sKey,const char *plainText);  
    std::string CFB_AESEncryptStr(const char *plainText);  
      
    /* 
    入口 
     sKey       钥匙 
     cipherText 密文 
    出口 
     明文  
    */  
    std::string CFB_AESDecryptStr(std::string sKey,const char *cipherText);  
    std::string CFB_AESDecryptStr(std::string cipherText);  
      
    /* 
    功能 
     CFB模式AES算法字符串加密，返回BASE64编码字符串，明文自动16字节对齐，使用0x30填充。 
    入口 
     key       钥匙，长度必须为 "16/24/32" 三者之一 
     plainText 明文 
    出口 
     密文  
    */  
    std::string CFB_AESEncryptStr_BASE64(const char *key,const char *plainText);  
      
    /* 
    功能 
     CFB模式AES算法字符串解密，对BASE64编码字符串进行解码 
    入口 
     key        钥匙，长度必须为 "16/24/32" 三者之一 
     cipherText 密文 
    出口 
     明文  
    */  
    std::string CFB_AESDecryptStr_BASE64(const char *key,const char *cipherText);  
      
      
    /* 
    功能 
     GCM模式AES算法字符串加密 
    入口: 
     sKey   钥匙。可以为空字符串。 
     sAuth  Auth（解码时必须）， 一般Auth存放明文的签名。可以为空字符串。 
     sPlain 明文 
    出口: 
     iv      增量（解码时必须） ，用于防止同样的钥匙和明文出现同样的密文。 
             原则上同样的IV，不能在同key上重复出现。 
     sCipher 密文 
    */  
    bool GCM_AESEncryptStr(const std::string &sKey,const std::string &sAuth,const std::string &sPlain,   
                           byte iv[16],std::string &sCipher);  
 
    /* 
    入口: 
     sKey    钥匙 
     sAuth   Auth（解码时必须）， 一般Auth存放明文的签名 
     sCipher 密文 
     iv      增量（解码时必须） 
    出口:  
     sPlain 明文  
    */  
    bool GCM_AESDecryptStr(const std::string &sKey,const std::string &sAuth,const std::string &sCipher,const byte iv[16],  
                           std::string &sPlain);  
    //////////////////////////////////////////////////////////  
    /* 
    入口: 
     msg    要产生文摘的内容 
    出口:  
     digest 文摘 
    */  
    void SHA256_Cal(const std::string &msg,std::string &digest);  
    /* 
    入口: 
     msg    内容 
     digest 文摘 
    出口: 
     校验成功返回 True 
    */  
    bool SHA256_Verify(const std::string &msg,const std::string &digest);  
    //////////////////////////////////////////////////////////  
    /* 
    入口 
     strSeed 种子，可以取值"seed"。 
    出口 
     strPri 私钥 解密用 
     strPub 公钥 加密用 
    */  
    void GenerateRSAKey(const std::string &strSeed,std::string &strPri, std::string &strPub);  
    /* 
    入口 
     strSeed   调用GenerateRSAKey时用的种子。 
     strPub    公钥 
     plainText 明文 
    出口 
      返回密文 
    */  
    std::string RSAEncryptStr(const std::string &strPub,const std::string &strSeed, const char *plainText);  
    /* 
    入口 
     strSeed    调用GenerateRSAKey时用的种子。 
     strPri     私钥 
     cipherText 密文 
    出口 
      返回明文 
    */  
    std::string RSADecryptStr(const std::string &strPri, const char *cipherText);  
    //////////////////////////////////////////////////////////  
