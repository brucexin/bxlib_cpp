#ifndef _CRYPTOPP_EASY_H
#define _CRYPTOPP_EASY_H

#include <string>
#include <vector>
#include <iostream>
#include "cryptopp_helper.h"
#include "bxlib_str.h"

/* 
    功能 
     GCM模式AES算法字符串加密的简易使用函数，特点是加密后只有一个数据输出。根据KEY和密文, 可以得到原文
*/  
class GCM_AES_Easy {
public:
	enum {_GCM_BLOCK_SIZE =	16};

	/* 
    功能 
		加密
    入口: 
     sKey   钥匙。。 
     
     sPlain 明文 
    出口: 
	 cipher
    */  
	static bool encrypt(const byte key[_GCM_BLOCK_SIZE], const std::string& sAuth, const std::string &sPlain, std::string& cipher) {
		byte iv[_GCM_BLOCK_SIZE];
		std::string output;

		if(!GCM_AESEncryptStr(std::string((const char*)key, _GCM_BLOCK_SIZE), sAuth, sPlain, iv, output)) {
			return false;
		}

		std::cout << "encrypt cipher hex:" << Bytes2Hex((byte*)output.c_str(), output.length()) << std::endl;
		std::cout << "encrypt iv hex:" << Bytes2Hex(iv, _GCM_BLOCK_SIZE) << std::endl;
		cipher = bxlib::r10_to_36((const unsigned char*)output.c_str(), output.length());
		cipher += bxlib::r10_to_36(iv, _GCM_BLOCK_SIZE);
		return true;
	}

	/* 
    功能 
		解密
    入口: 
     sKey   钥匙。。 
     
     sCipher 密文 
    出口: 
	 plain 明文
    */  
	static bool decrypt(const byte key[_GCM_BLOCK_SIZE], const std::string& sAuth, const std::string &sCipher, std::string& plain) {
		byte iv[_GCM_BLOCK_SIZE];
		
		//长度都不够auth和iv的编码
		if( sCipher.length() < _GCM_BLOCK_SIZE*2*2 ) {
			return false;
		}

		std::string siv = sCipher.substr(sCipher.length()-_GCM_BLOCK_SIZE*2);
		std::string cipher = sCipher.substr(0, sCipher.length()-_GCM_BLOCK_SIZE*2);
		if(!bxlib::r36_to_10(siv, iv)) {
			return false;
		}

		std::vector<byte> buf(cipher.length()/2);
		if(!bxlib::r36_to_10(cipher, buf.data())) {
			return false;
		}

		std::cout << "decrypt cipher hex:" << Bytes2Hex(buf.data(), buf.size()) << std::endl;
		std::cout << "decrypt cipher iv:" << Bytes2Hex(iv, _GCM_BLOCK_SIZE) << std::endl;

		if(!GCM_AESDecryptStr(std::string((char*)key, _GCM_BLOCK_SIZE), sAuth, 
			std::string((const char*)buf.data(), buf.size()), iv, plain)) {
			return false;
		}

		return true;
	}
};

//link bxlib_str lib for radix 36
#pragma   comment(lib,"bxlib_str.lib")

#endif //_CRYPTOPP_EASY_H