#include "stdafx.h"

#include "osrng.h"
using CryptoPP::AutoSeededRandomPool;
#include "cryptlib.h"
using CryptoPP::Exception;

#include "hmac.h"
using CryptoPP::HMAC;

#include "sha.h"
using CryptoPP::SHA256;

#include "filters.h"
using CryptoPP::StringSink;
using CryptoPP::StringSource;
using CryptoPP::StreamTransformationFilter;

#ifndef BXLIB_USE_INLINE
#	include "digest.h"
#endif

namespace BXLIB_NS {

void hmac(const std::string& input, std::string& key, std::string& output, size_t key_size) {
	AutoSeededRandomPool prng;
	CryptoPP::SecByteBlock hmac_key(key_size);
	prng.GenerateBlock(hmac_key, hmac_key.size());
	CryptoPP::HMAC< SHA256 > hmac(hmac_key, hmac_key.size());
	std::string mac;

	CryptoPP::StringSource(&input[0], true, 
		new CryptoPP::HashFilter(hmac,
			new CryptoPP::StringSink(mac)
		) // HashFilter      
	); // StringSource
	output = mac;
	key.assign((char*)hmac_key.BytePtr(), hmac_key.size());
}


}