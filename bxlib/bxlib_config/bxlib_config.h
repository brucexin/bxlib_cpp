#ifndef _BXLIB_CONFIG_H

#ifdef BXLIB_USE_INLINE

#define BXLIB_INLINE inline
//include source file when use inline
#define BXLIB_INLINE_INCLUDE(srcfile)	#include srcfile

#else

#define BXLIB_INLINE 
#define BXLIB_INLINE_INCLUDE(srcfile)	

#endif


#define BXLIB_NS	bxlib



#endif //_BXLIB_CONFIG_H