#ifndef _BXLIB_TYPE_H
#define _BXLIB_TYPE_H

#ifndef BYTE
#	define BYTE		unsigned char
#endif

#ifndef UINT
#	define UINT		unsigned int
#endif

#ifndef ULONG
#	define ULONG	unsigned long
#endif


#ifndef BX_OUT
#	define BX_OUT
#endif

#ifndef BX_IN
#	define BX_IN
#endif


#endif //_BXLIB_TYPE_H